This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Hello world

Clone and install dependencies:

```
git clone https://bitbucket.org/goodok21/post-and-comment.git
cd post-and-comment
npm install
yarn install

```

React frontend:
```
npm start

```

Node.js backend:
```
npm run server

```

### You can use my mongodb or create your own at mlab.com and configure ./server/config/db.js
