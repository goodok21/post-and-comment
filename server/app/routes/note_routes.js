// post_routes.js
var ObjectID = require('mongodb').ObjectID
module.exports = function(app, db) {

  // React static files after build
  app.get('/', (req, res) => {
    res.send('hello')
  })


  // Posts server side

  app.get('/posts/', (req, res) => {
    db.collection('posts').find().toArray().then( (response) => {
      res.send(response)
    })
  })

  app.get('/posts/:id', (req, res) => {
    const id = req.params.id
    const details = { '_id': new ObjectID(id) }
    db.collection('posts').findOne(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'})
      } else {
        res.send(item)
      }
    })
  })

  app.post('/posts/', (req, res) => {
    console.log(req.body);
    const post = { text: req.body.text, title: req.body.title, date: Date.now() }
    db.collection('posts').insert(post, (err, result) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' })
      } else {
        res.send(result.ops[0])
      }
    })
  })

  app.delete('/posts/:id', (req, res) => {
    const id = req.params.id
    const details = { '_id': new ObjectID(id) }
    db.collection('posts').remove(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'})
      } else {
        res.send('Post ' + id + ' deleted!')
      }
    })
  })

  app.put('/posts/:id', (req, res) => {
    const id = req.params.id
    const details = { '_id': new ObjectID(id) }
    const post = { text: req.body.text, title: req.body.title, date: req.body.date }
    db.collection('posts').update(details, post, (err, result) => {
      if (err) {
          res.send({'error':'An error has occurred'})
      } else {
          res.send(post)
      }
    })
  })

  // ... and again
  // Comments server side

  app.get('/:id/comments/', (req, res) => {
    const id = req.params.id
    const details = { 'post_id': id }
    db.collection('comments').find( details ).toArray().then( (response) => {
      res.send(response)
    })
  })

  app.get('/comments/:id', (req, res) => {
    const id = req.params.id
    const details = { '_id': new ObjectID(id) }
    db.collection('comments').findOne(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'})
      } else {
        res.send(item)
      }
    })
  })

  app.post('/:id/comments/', (req, res) => {
    const id = req.params.id
    const details = { 'post_id': id }
    const comment = { post_id: id, text: req.body.text, date: Date.now() }
    db.collection('comments').insert(comment, (err, result) => {
      if (err) {
        res.send({ 'error': 'An error has occurred' })
      } else {
        res.send(result.ops[0])
      }
    })
  })

  app.delete('/comments/:id', (req, res) => {
    const id = req.params.id
    const details = { '_id': new ObjectID(id) }
    db.collection('comments').remove(details, (err, item) => {
      if (err) {
        res.send({'error':'An error has occurred'})
      } else {
        res.send('comment ' + id + ' deleted!')
      }
    })
  })

  app.put('/comments/:id', (req, res) => {
    const id = req.params.id
    const details = { '_id': new ObjectID(id) }
    const comment = { post_id: req.body.post_id, text: req.body.text, date: req.body.date }
    db.collection('comments').update(details, comment, (err, result) => {
      if (err) {
          res.send({'error':'An error has occurred'})
      } else {
          res.send(result)
      }
    })
  })

}
