import React, {
  Component,
  PropTypes,
} from 'react'

import Post from './Post'
import Api from './Api'

export default class ListPosts extends Component {

  static defaultProps = {}

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {
      posts: []
    };
  }

  componentDidMount() {
    Api.ListPosts()
      .then( (response) => {
        this.setState({
          posts: response
        })
      })
  }

  render() {
    return (
      <div>
        { this.state.posts.map( post => { return <Post post={post} key={post._id} /> } ) }
      </div>
    );
  }

}
