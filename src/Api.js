import axios from 'axios';
import qs from 'qs'

export default class {
  constructor() {

  }

  static ListPosts () {
    return new Promise( (resolve, reject) => {
      axios.get('http://localhost:8000/posts/')
      .then(response => {
        const posts = response.data;
        resolve(posts)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }

  static GetPost (id) {
    return new Promise( (resolve, reject) => {
      axios.get('http://localhost:8000/posts/' + id)
      .then(response => {
        const post = response.data
        resolve(post)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }

  static AddPost (post) {
    return new Promise( (resolve, reject) => {
      axios.post('http://localhost:8000/posts/',
        qs.stringify({
          title: post.title,
          text: post.text
        })
      )
      .then( (response) => {
        const post = response.data
        resolve(post)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }

  static EditPost (post) {
    return new Promise( (resolve, reject) => {
      axios.put('http://localhost:8000/posts/' + post._id,
        qs.stringify({
          date: post.date,
          title: post.title,
          text: post.text
        })
      )
      .then( (response) => {
        resolve(response)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }

  static DeletePost (id) {
    return new Promise( (resolve, reject) => {
      axios({
        method: 'DELETE',
        url: 'http://localhost:8000/posts/'+id
      })
      .then(response => {
        resolve(response)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }


  // Comments

  static ListComments (post_id) {
    return new Promise( (resolve, reject) => {
      axios.get('http://localhost:8000/'+post_id+'/comments')
      .then(response => {
        const comments = response.data;
        resolve(comments)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }

  static AddComment (comment) {
    return new Promise( (resolve, reject) => {
      axios.post('http://localhost:8000/'+comment.post_id+'/comments',
        qs.stringify({
          text: comment.text
        })
      )
      .then( (response) => {
        const comment = response.data
        resolve(comment)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }

  static EditComment (comment) {
    return new Promise( (resolve, reject) => {
      axios.put('http://localhost:8000/comments/' + comment._id,
        qs.stringify({
          post_id: comment.post_id,
          date: comment.date,
          text: comment.text
        })
      )
      .then( (response) => {
        resolve(response.data)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }

  static DeleteComment (id) {
    return new Promise( (resolve, reject) => {
      axios({
        method: 'DELETE',
        url: 'http://localhost:8000/comments/'+id
      })
      .then(response => {
        resolve(response)
      })
      .catch((ex)=>{ console.log(ex) })
    })
  }

}
