import React, {
  Component,
  PropTypes,
} from 'react';

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

export default class Post extends Component {

  static defaultProps = {}

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {};
  }

  formatDate = (unixtime) => {
    var options = {
      hour: 'numeric',
      minute: 'numeric',
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    }
    return unixtime ? new Intl.DateTimeFormat('ru-RU', options).format(unixtime) : 'Нет такого времени'
  }

  render() {
    return (
      <div className="card mb-3">
        <div className="card-block">
          <h5 className="card-title">{this.props.post.title}</h5>
          <h6 className="card-subtitle mb-2 text-muted">{ this.formatDate( this.props.post.date ) }</h6>
          <p className="card-text">{this.props.post.text}</p>
          <Link className="btn btn-primary" to={ String(this.props.post._id) }>Read more</Link>
          <Link className="btn btn-primary ml-3" to={ "/editpost/"+String(this.props.post._id) }>Edit</Link>
        </div>
      </div>
    );
  }

}
