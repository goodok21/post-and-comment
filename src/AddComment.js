import React, {
  Component,
  PropTypes,
} from 'react';

import Api from './Api'

export default class AddComment extends Component {

  static defaultProps = {}

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState( {value: event.target.value} );
  }

  handleSubmit(event) {
    if (this.state.value != '') {
      var comment = {
        post_id: this.props.post_id,
        text: this.state.value
      }
      this.props.onHandleAdd(comment)
      this.setState({ value: '' })
    }
    event.preventDefault()
  }

  render() {
    return (
      <div className="card-block">
        <form className="form-group" onSubmit={this.handleSubmit}>
          <textarea className="form-control" rows="3" value={this.state.value} onChange={this.handleChange} placeholder="Type your comment here"></textarea>
          <button type="submit" className="btn btn-primary mt-3">Submit</button>
        </form>
      </div>
    );
  }

}
