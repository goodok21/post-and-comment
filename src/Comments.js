import React, {
  Component,
  PropTypes,
} from 'react';

import Api from './Api'
import Comment from './Comment'
import AddComment from './AddComment'

export default class Comments extends Component {

  static defaultProps = {
    post_id: ''
  }

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {
      comments: []
    };

    this.AddComment = this.AddComment.bind(this)
    this.RefreshComments = this.RefreshComments.bind(this)
    this.DeleteComment = this.DeleteComment.bind(this)
    this.EditComment = this.EditComment.bind(this)

  }

  AddComment (comment) {
    console.log(comment);
    comment && (
      Api.AddComment(comment)
      .then(comment => {
        this.RefreshComments(this.props.post_id)
      })
    )
  }

  RefreshComments (post_id) {
    Api.ListComments(post_id)
      .then(comments => {
        console.log(comments);
        this.setState({ comments })
      })
  }

  EditComment (comment) {
    Api.EditComment(comment)
      .then(comment=>{
        console.log(comment);
        this.RefreshComments(this.props.post_id)
      })

  }

  DeleteComment (id) {
    Api.DeleteComment(id)
      .then(response => {
        this.RefreshComments(this.props.post_id)
      })
  }

  componentDidMount () {
    this.RefreshComments(this.props.post_id)
  }

  render() {
    return (
      <div className="card mb-3">
        <div className="card-block">
          <h4 className="card-title">Comments</h4>
          { this.state.comments.map( comment => { return <Comment comment={comment} key={comment._id} onHandleDel={this.DeleteComment} onHandleEdit={this.EditComment} /> } ) }
        </div>

        <AddComment post_id={this.props.post_id} onHandleAdd={this.AddComment} />

      </div>

    );
  }

}
