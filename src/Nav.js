import React, {
  Component,
  PropTypes,
} from 'react';

import {
  NavLink,
  Link
} from 'react-router-dom'

export default class Nav extends Component {

  static defaultProps = {}

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <nav className="navbar navbar-toggleable-md navbar-light pl-0 pr-0">
        <NavLink className="navbar-brand" to={"/"}><h1>Post&amp;Comment</h1></NavLink>
        <div className="navbar-nav mr-auto">
          <NavLink className="nav-link" to={"/"}>List Posts</NavLink>
          <NavLink className="nav-link" to={"/addpost/"}>Add post</NavLink>
        </div>
      </nav>
    );
  }



}
