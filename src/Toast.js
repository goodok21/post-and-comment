import React, {
  Component,
  PropTypes,
} from 'react';
import { Link } from 'react-router-dom'

export default class Toast extends Component {

  static defaultProps = {
    id: '/',
    text: 'Saved!',
    subtext: 'Post saved and now you can see this magic:'
  }

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    console.log(this.props);
    return (
      <div className="alert alert-success" role="alert">
        <strong>{this.props.text}</strong><br/>
        <p>{this.props.subtext}</p><Link to={"/" + this.props.id}>View</Link>
      </div>
    );
  }

}
