import React, {
  Component,
  PropTypes,
} from 'react';
import Api from './Api'
import Comments from './Comments'

export default class PostDetails extends Component {

  constructor(props) {
    super(props)
    this.state = {
      post: {
        _id: '',
        title: '',
        text: ''
      }
    }
  }

  formatDate = (unixtime) => {
    var options = {
      hour: 'numeric',
      minute: 'numeric',
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    }
    return unixtime ? new Intl.DateTimeFormat('ru-RU', options).format(unixtime) : 'Нет такого времени'
  }

  componentDidMount() {
    const id = this.props.match.params.id
    Api.GetPost(id)
      .then( (post) => {
        this.setState({post})
      })

  }

  render() {
    return (
      <div>
        <h2 className="card-title">{this.state.post.title}</h2>
        <h6 className="card-subtitle mb-2 text-muted">{this.formatDate(this.state.post.date)}</h6>
        <p className="card-text">{this.state.post.text}</p>
        <Comments post_id={this.props.match.params.id}/>
      </div>
    );
  }

}
