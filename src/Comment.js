import React, {
  Component,
  PropTypes,
} from 'react';

export default class Comment extends Component {

  static defaultProps = {
    comment: {
      _id: '',
      date: '',
      text: ''
    }
  }

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {
      value: this.props.comment.text,
      edit: false
    };

    this.handleEdit = this.handleEdit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDelete = this.handleDelete.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
  }

  handleEdit () {
    this.setState({
      edit: true
    })
    console.log(this.state);
  }

  handleCancel (e) {
    this.setState({
      edit: false
    })
    e.preventDefault()
    console.log('cancel');
  }

  handleChange (e) {
    this.setState( {value: e.target.value} )
  }

  handleSubmit (e) {
    var comment = this.props.comment
    comment.text = this.state.value
    this.props.onHandleEdit(comment)
    this.setState({
      edit: false
    })
    e.preventDefault()
  }

  handleDelete () {
    this.props.onHandleDel(this.props.comment._id)
  }

  render() {
    return (
      <div className="card mb-3">
        <div className="card-block">
          { this.state.edit ? (
            <form className="form-group" onSubmit={this.handleSubmit}>
              <textarea className="form-control" rows="3" value={this.state.value} onChange={ this.handleChange }></textarea>
              <button type="submit" className="btn btn-primary mt-3">Submit</button>
              <button className="btn btn-primary mt-3 ml-3" onClick={ this.handleCancel }>Cancel</button>
            </form>
          ) : (
            <p className="card-text" onClick={ ()=>{ this.handleEdit() } }>
              {this.props.comment.text}
            </p>
          )}
          <button className="btn btn-primary mt-3 card-link" onClick={ ()=>{ this.handleDelete() } }>Delete</button>
        </div>
      </div>
    );
  }

}
