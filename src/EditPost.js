import React, { Component, PropTypes } from 'react';
import { Link, Redirect } from 'react-router-dom'

import Api from './Api'

import Toast from './Toast'

export default class EditPost extends Component {

  static defaultProps = {}

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {
      saved: false,
      post: {
        _id: '',
        title: '',
        text: ''
      }
    }

    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeText = this.handleChangeText.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleChangeTitle(event) {
    var post = this.state.post
    post.title = event.target.value
    this.setState({ post })
  }

  handleChangeText(event) {
    var post = this.state.post
    post.text = event.target.value
    this.setState({ post })
  }

  handleSubmit(event) {
    var post = this.state.post
    Api.EditPost(post)
      .then( (response) => {
        this.setState({ saved: true })
      })
    event.preventDefault()
  }

  handleDelete (id) {
    Api.DeletePost(id)
      .then(res => {
        this.setState({ redirect: true })
      })
  }

  componentDidMount () {
    Api.GetPost(this.props.match.params.id)
      .then( (post) => {
        this.setState({ post })
      })
  }

  render() {
    return (
      <div className="card-block">
        <form className="form-group" onSubmit={this.handleSubmit}>
          <input className="form-control" type="text" value={this.state.post.title} onChange={this.handleChangeTitle} placeholder="The title"/>
          <textarea className="form-control mt-3" rows="3" value={this.state.post.text} onChange={this.handleChangeText} placeholder="Post content"></textarea>
          <button type="submit" className="btn btn-primary mt-3">Submit</button>
        </form>
        <button className="btn btn-link" onClick={ ()=>this.handleDelete(this.props.match.params.id) }>Delete</button>
        { this.state.redirect ? <Redirect to={{ pathname: '/', state: { from: this.props.location } }}/> : null}

        { this.state.saved ? <Toast id={this.state.post._id} text={"Saved"} subtext={"See magic"}/> : null }
      </div>
    );
  }

}
