import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom'

// Components
import Nav from './Nav'
import ListPosts from './ListPosts'
import PostDetails from './PostDetails'
import AddPost from './AddPost'
import EditPost from './EditPost'

const App = () => (
  <Router>
    <div className="container pt-3 pb-3">
      <Nav />

      <Switch>
        <Route path="/addpost/" component={AddPost} />
        <Route path="/editpost/:id" component={EditPost} />
        <Route path="/:id" component={PostDetails} />

        <Route component={ListPosts} />
      </Switch>
    </div>
  </Router>
)

// const Post = ({ match }) => (
//   <div>
//     <h3>ID: {match.params.id}</h3>
//   </div>
// )

export default App
