import React, {
  Component,
  PropTypes,
} from 'react';
import Toast from './Toast'
import Api from './Api'

// The main class
export default class AddPost extends Component {

  static defaultProps = {}

  static propTypes = {}

  constructor(props) {
    super(props);
    this.state = {
      saved: false,
      post: {
        _id: '',
        title: '',
        text: ''
      }
    }

    this.handleChangeTitle = this.handleChangeTitle.bind(this);
    this.handleChangeText = this.handleChangeText.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChangeTitle(event) {
    var post = this.state.post
    post.title = event.target.value
    this.setState({ post })
  }

  handleChangeText(event) {
    var post = this.state.post
    post.text = event.target.value
    this.setState({ post })
  }

  handleSubmit(event) {
    var post = this.state.post
    Api.AddPost(post)
      .then( (post) => {
        this.setState({ saved :true, post })
      })
    event.preventDefault()
  }

  render() {
    return (
      <div className="card-block">
        <form className="form-group" onSubmit={this.handleSubmit}>
          <input className="form-control" type="text" value={this.state.post.title} onChange={this.handleChangeTitle} placeholder="The title"/>
          <textarea className="form-control mt-3" rows="3" value={this.state.post.text} onChange={this.handleChangeText} placeholder="Post content"></textarea>
          <button type="submit" className="btn btn-primary mt-3">Submit</button>
        </form>
        { this.state.saved ? <Toast id={this.state.post._id} /> : null }
      </div>
    );
  }

}
